﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class BeeGenerate : MonoBehaviour
{
    // Current game mode
    int loadedGameMode;

    // Queen location
    [SerializeField] List<Vector2> queenTrack = null;

    // Video objects
    GameObject frameVideo;
    VideoPlayer player;

    // Start is called before the first frame update
    void Start()
    {
        // Load frame objects
        frameVideo = GameObject.Find("FrameVid");
        player = frameVideo.GetComponent<VideoPlayer>();

        loadedGameMode = PlayerPrefs.GetInt("gameMode", -1);
        if (loadedGameMode == 0)
        {
            Debug.Log("Play mode!");

            // Initialise video settings
            player.playOnAwake = true;
            player.renderMode = VideoRenderMode.MaterialOverride;
            player.isLooping = true;
            player.playbackSpeed = 1.0f;
            player.url = "Assets/Videos/Honey Bee Hive.mp4";
            player.Play();
        }
        else if (loadedGameMode == 1)
        {
            Debug.Log("Record mode!");

            // Initialise video settings
            player.playOnAwake = false;
            player.renderMode = VideoRenderMode.MaterialOverride;
            player.isLooping = false;
            player.playbackSpeed = 0.0f;
            player.url = "Assets/Videos/Honey Bee Hive.mp4";
            player.Play();
        }
        else
        {
            Debug.Log("The scene has been improperly loaded!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Record Mode
        if (loadedGameMode == 1)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                StartCoroutine("capturePosition");
            }

            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                StopCoroutine("capturePosition");
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                StartCoroutine("deleteCapturedPositions");
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                StopCoroutine("deleteCapturedPositions");
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                SaveData.SaveQueenTrack(player, queenTrack);
            }
        }
    }

    IEnumerator capturePosition()
    {
        while (true)
        {
            queenTrack.Add(Input.mousePosition);
            player.frame++;
            player.Play();

            player.loopPointReached += endOfVideoTrigger;
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator deleteCapturedPositions()
    {
        while (true)
        {
            queenTrack.RemoveAt(queenTrack.Count - 1);
            player.frame--;
            player.Play();
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void endOfVideoTrigger(VideoPlayer vp)
    {
        Debug.Log("This is a hoot!");
    }
}
