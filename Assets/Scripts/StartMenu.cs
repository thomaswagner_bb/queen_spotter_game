﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    public void Play()
    {
        PlayerPrefs.SetInt("gameMode", 0);
        SceneManager.LoadScene("Game");
    }

    public void Record()
    {
        PlayerPrefs.SetInt("gameMode", 1);
        SceneManager.LoadScene("Game");
    }
}
