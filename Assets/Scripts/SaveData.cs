﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveData
{
    public static void SaveQueenTrack(VideoPlayer player,
                                      List<Vector2> queenTrack)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string filename = "/queenPos.dat";
        string path = Application.dataPath + filename;
        FileStream filestream = new FileStream(path, FileMode.Create);

        VideoData queenData = new VideoData(player);
        queenData.AddQueenTrack(queenTrack);

        formatter.Serialize(filestream, queenData);

        filestream.Close();
    }

    public static VideoData LoadQueenTrack()
    {
        string filename = "/queenPos.dat";
        string path = Application.dataPath + filename;

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            FileStream filestream = new FileStream(path, FileMode.Open);

            VideoData queenData = formatter.Deserialize(filestream) as VideoData;
            filestream.Close();

            return queenData;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
}
