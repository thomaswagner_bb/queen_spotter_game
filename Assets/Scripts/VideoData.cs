﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[System.Serializable]
public class VideoData
{
    VideoPlayer cplayer;
    float[] position;

    public VideoData(VideoPlayer player)
    {
        cplayer = player;
    }

    public void AddQueenTrack(List<Vector2> track)
    {
        position = new float[2 * track.Count];

        int i = 0;
        foreach (Vector2 pos in track)
        {
            position[i] = pos.x;
            position[i + 1] = pos.y;
            i += 2;
        }
    }
}
